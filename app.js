
const express = require('express');
const app = express();
const morgan = require('morgan');
app.use(express.json())

console.log(process.env.NODE_ENV)

if(process.env.NODE_ENV === 'development'){
    app.use(morgan('dev'))
}

app.use(express.static(`${__dirname}/public`))
app.use((req,res,next)=>{
req.requestTime = new Date().toISOString();
console.log(req.headers);
next();
})

const tourRouter = require('./routes/tourRouter')
const userRouter = require('./routes/userRouter');
const AppError = require('./utils/appError');
const globalErrorHandler = require('./controllers/errorControler');


app.use('/api/v1/tours',tourRouter)
app.use('/api/v1/users',userRouter)

app.all('*',(req,res,next)=>{
    // res.status(404).json({
    //     status: 'fail',
    //     message:`Can't find this ${req.originalUrl} on this server! `
    //   });

    // const err = new Error(`Can't find this ${req.originalUrl} on this server! `)
    // err.statusCode = 404 ;
    // err.status = 'fail';
    next(new AppError(`Can't find this ${req.originalUrl} on this server! `,404));
})

app.use(globalErrorHandler)

module.exports = app;

const express = require('express');
const fs = require('fs');
const tourController = require('./../controllers/tourController');
const authControler = require('./../controllers/authController')
const router = express.Router();

// router.param('id', tourController.checkId)

// check body middleware 
// check if the body contain name and price property
// if not send back 400 sttus code i.e ( bad request )
// add it to post handlear stack 
router.route('/top-5-cheap').get(tourController.aliasTopTours,tourController.getAllTours)
router.route('/tour-stats').get(tourController.getTourStats)


router.route('/')
.get(authControler.protect, tourController.getAllTours)
.post(tourController.createTour)

router.route('/:id')
.get(tourController.getTour)
.patch(tourController.updateTour)
.delete(authControler.protect,authControler.restrictTo('admin','lead-guide'),tourController.deleteTour)



module.exports = router ;
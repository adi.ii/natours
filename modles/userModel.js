const crypto = require('crypto');
const mongoose = require('mongoose');
const validator = require('validator');
const bcrypt = require('bcryptjs');

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Please tell us your name!']
    },
    email: {
        type: String,
        required: [true, 'Please provide your email'],
        unique: true,
        lowercase: true,
        validator: [validator.isEmail, 'Please provide a valid email']
    },
    photo: String,
    role: {
        type: String,
        enum: ['user', 'guide', 'lead-guide', 'admin'],
        default: 'user'
    },
    password: {
        type: String,
        required: [true, 'Please provide a password'],
        minLength: 8,
        select: false
    },
    passwordConfirm: {
        type: String,
        required: [true, 'Please confirm your password'],
        //This only work on Create & Save !!
        validate: {
            validator: function (el) {
                return el === this.password;
            },
            message: 'password are not same'
        }
    },
    passwordChangedAt: Date,
    passwordResetToken: String,
    passwordRestExpires: Date
});

userSchema.pre('save', async function (next) {

    // Only run this function if password was actually modify
    if (!this.isModified('password')) return next();

    this.password = await bcrypt.hash(this.password, 12);
    this.passwordConfirm = undefined;
    next();
});

userSchema.methods.correctPassword = async function (candidatePassword, userPasssword) {
    return await bcrypt.compare(candidatePassword, userPasssword);
}

userSchema.methods.changedPasswordAfter = function (JWTTimestamp) {
    if (this.passwordChangedAt) {
        const changedTimestamp = parseInt(this.passwordChangedAt.getTime() / 1000, 10);
        // console.log(this.passwordChangedAt , JWTTimestamp)
        return JWTTimestamp < changedTimestamp
    }

    // false means not changed
    return false;
}

userSchema.methods.createPasswordRestToken = function () {
    const resetToken = crypto.randomBytes(32).toString('hex');
    this.passwordResetToken = crypto
        .createHash('sha256')
        .update(resetToken)
        .digest('hex');

    this.passwordRestExpires = Date.now() + 10*60*1000;
    return resetToken;
}

const User = mongoose.model('User', userSchema);
module.exports = User;
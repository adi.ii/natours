const nodemailer = require('nodemailer');


const sendEmail = async options =>{
// 1) create a transporter
 const transporter = nodemailer.createTransport({
    host:'sandbox.smtp.mailtrap.io',
    auth:{
        user:'bde24a5d42db6c',
        pass: '3a3ebe7988e0f2'
    }
    // Activate in gmail "less secure app " option
 })

// 2) Define the email options

const mailOptions = {
    from : 'Aditya Modak <aditya@zagzag.in>',
    to: options.email,
    subject: options.subject,
    text: options.message,
}

// 4) Actually send the email
await transporter.sendMail(mailOptions);

}

module.exports = sendEmail;
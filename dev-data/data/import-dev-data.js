const fs = require('fs')
const mongoose = require("mongoose");
const dotenv = require('dotenv');
const Tour = require('./../../modles/tourModle');

dotenv.config({ path: './config.env' })
const connectDB = async () => {
    try {
        await mongoose.connect("mongodb://127.0.0.1:27017/natours", {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            
        });
        console.log("DB connected")
    } catch (error) {
        console.error(error.message)
    }
}
connectDB();

const tour = JSON.parse(fs.readFileSync(`${__dirname}/tours-simple.json`, 'utf-8'));

const importData = async () => {
    try {
        await Tour.create(tour)
        console.log("Tour created successfully")
    } catch (err) {
        console.log(err)
    }
    process.exit();
}

const deleteData = async()=>{
    try {
        await Tour.deleteMany()
        console.log("Tour deleted successfully")
    } catch (err) {
        console.log(err)
    }
    process.exit();
}

try{
    if(process.argv[2] === '--import'){
        importData();
    }else if(process.argv[2] === '--delete'){
        deleteData()
    }
}catch(err){
console.log("error here",err)
}

console.log(process.argv)
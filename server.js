const mongoose = require("mongoose");
const dotenv = require('dotenv');
dotenv.config({ path: './config.env' })

const connectDB = async () => {
    try {
        await mongoose.connect("mongodb://127.0.0.1:27017/natours",{
            useNewUrlParser: true,
            useUnifiedTopology: true
        });
        console.log("DB connected")
    } catch (error) {
        console.error(error.message)
    }
}
connectDB()

// process.on('unhandledRejection', err =>{
//     server.close(()=>{
//         process.exit(1);
//     })
    
// })

const app = require('./app')
const port = process.env.PORT || 3000;

app.listen(port, () => {
    console.log(`app runing at port ${port}...`)
})
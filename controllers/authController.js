const {promisify} = require('util');
const jwt = require('jsonwebtoken');
const User = require('./../modles/userModel');
const catchAsync = require('./../utils/catchAsync');
const AppError = require('../utils/appError');
const sendEmail = require('./../utils/email');

const signToken = id =>{
    return jwt.sign({id}, 'hi_you-can-call-me-hero@123-or-king#123_secret-hai',{

    });
}




exports.signup = catchAsync(async(req,res,next)=>{
    const newUser = await User.create(req.body);
    const token = signToken(newUser._id)
    res.status(201).json({
        status:'success',
        token,
        data:{
            user:newUser
        }
    })
});

exports.login = catchAsync( async(req,res,next)=>{

  const {email,password} = req.body;
  if(!email ||!password){
    next(new AppError('please provide email and password!',400))
  }
  const user= await User.findOne({email}).select('+password');

 if(!user || !(await user.correctPassword(password,user.password))){
    return next(new AppError('Incorrect email or password',401))
 }
  const token = signToken(user._id);
  res.status(200).json({
    status:'success',
    token
  })
});


exports.protect = catchAsync(async(req,res,next)=>{
// 1) Getting token and check of it's there
let token;
if(req.headers.authorization && req.headers.authorization.startsWith('Bearer')){
     token = req.headers.authorization.split(' ')[1];
}

if(!token){
return next(new AppError('You are not logged in!',401));
}
// 2) Verification token
const decoded = await promisify(jwt.verify)(token,'hi_you-can-call-me-hero@123-or-king#123_secret-hai');
// console.log(decoded)

// 3) check if user still exist
const currentUser = await User.findById(decoded.id);

if(!currentUser){
  return next(new AppError('The user belonging to this token does no longer exist.',401))
}
// 4) Check if user change password after the token was issue

 if(currentUser.changedPasswordAfter(decoded.iat)){
return next(new AppError('User recently changedpassword! Please log in again.',401))
 }
// grant access to protected route
req.user = currentUser;
 next();
})

exports.restrictTo = (...roles) =>{
  return (req,res,next)=>{
    if(!roles.includes(req.user.role)){
      return next(new AppError('You do not have permission to perform this action',403))
    }
    next();
  }
}

exports.forgotPassword = catchAsync(async(req,res,next)=>{

 const user = await User.findOne({email:req.body.email});
 console.log("===== user ===",user)
 
 if(!user){
  return next(new AppError('There is no user with this email address.',404));
 }

 const resetToken = user.createPasswordRestToken();
 await user.save({validateBeforeSave:false});

 const resetURL = `${req.protocol}://${req.get('host')}/api/v1/users/resetPassword/${resetToken}`;

 const message = `Forget your password? Submit a Patch request with your new password and passwordConform to:${resetURL}. \n if 
 you didn't forget your password , please ignore this email!
 `
 try{
  await sendEmail({
    email:user.email,
    subject:'Your password reset token ( valid for 10 min )',
    message
  });
  
  res.status(200).json({
    status:'success',
    message:'Token sent to email'
  })
 }catch(err){
  user.passwordResetToken = undefined;
  user.passwordResetExpires = undefined;
  await user.save({validateBeforeSave:false});

  return next(new AppError('There was an error sending the email, Try again Later!',500))
 }

});

exports.resetPassword = (req,res,next)=>{

}
const AppError = require("./../utils/appError");


const handelCastError = err =>{
   const message = `Invilid ${err.path}: ${err.value}` ;
   return new AppError(message,400) 
}

const handelDuplicateValueDB = err =>{
const value = err.errmsg.match(/(["'])(\\?.)*?\1/)[0];
const message = `Duplicate value : ${value}, please use another value .`;
return new AppError(message,400)
}

const handelValidationErrorDB = err =>{
    const errors = Object.values(er.errors).map(el=>el.message)
const message = `Invlid input data: ${errors.join('. ')}` ;
return new AppError(message , 400)
}

const handleJWTError = err => new AppError('Invlid token. Please login again!',401);
const handleJWTExpiredError = err => new AppError('Your token has expired. Please login again',401);

const sendErrorDev = (err,res)=>{
    res.status(err.statusCode).json({
        status: err.status,
        message:err.message,
        stack : err.stack
      });
}

const sendErrorPro = (err,res)=>{
    if(err.isOperational){
        res.status(err.statusCode).json({
            status: err.status,
            message:err.message,
        });

    }else{
        res.status(500).json({
            status: 'fail',
            message:'Something went wrong',
        }); 
    }
}

module.exports =(err,req,res,next)=>{
    err.statusCode = err.statusCode || 500;
    err.status = err.status || 'error'

    if(process.env.NODE_ENV === 'development'){
        sendErrorDev(err,res)
    }
    else if(process.env.NODE_ENV === 'production'){
        let error = {...err}
        if(error.name === 'CastError')  error = handelCastError(error)
        
        if(error.code === 11000) error = handelDuplicateValueDB(error)

        if(error.name === 'ValidationError')  error = handelValidationErrorDB(error)
          
        if(error.name === 'JsonWebTokenError') error = handleJWTError(error)
           
        if(error.name === 'TokenExpiredError')  error = handleJWTExpiredError(error)
           
        sendErrorPro(error,res)
    }

};